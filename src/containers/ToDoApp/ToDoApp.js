import React, {useEffect, useState} from 'react';
import ToDoForm from "../../components/ToDoForm/ToDoForm";
import ToDoList from "../../components/ToDoList/ToDoList";
import {useDispatch, useSelector} from "react-redux";
import {axiosPost, axiosGet, axiosRemove} from "../../store/actions";

const ToDoApp = () => {
    const dispatch = useDispatch();
    const tasks = useSelector(state => state.tasks);
    const [inputVal, setInputVal] = useState("");

    const addNewTask = (e) => {
        e.preventDefault();
        if (inputVal.length > 0) {
            dispatch(axiosPost({task: inputVal}));
        }

        setInputVal("");
    };

    const removeTask = (id) => dispatch(axiosRemove(id));

    const onChangeHandle = e => (setInputVal(e.target.value));

    useEffect(() => {
        dispatch(axiosGet());
    }, [dispatch]);

    return (
        <>
            <ToDoForm onChange={onChangeHandle} value={inputVal} onSubmit={addNewTask}/>
            <ToDoList tasks={tasks} onClick={removeTask}/>
        </>
    );
};

export default ToDoApp;