import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {getCounter, putCounter} from "../../store/actions";

const Counter = () => {
    const dispatch = useDispatch();
    const counter = useSelector(state => state.counter);

    const handleIncrement = () => (dispatch(putCounter(counter + 1)));
    const handleDecrement = () => (dispatch(putCounter(counter - 1)));
    const handleIncrementFive = () => (dispatch(putCounter(counter + 5)));
    const handleDecrementFive = () => (dispatch(putCounter(counter - 5)));

    useEffect(() => {
        dispatch(getCounter())
    }, [dispatch])

    console.log(counter)
    return (
        <div className="Counter">
            <h1>{counter}</h1>
            <button
                className="btn btn-primary m-2"
                onClick={handleIncrement}
            >
                Increase
            </button>
            <button
                className="btn btn-primary m-2"
                onClick={handleDecrement}
            >
                Decrease
            </button>
            <button
                className="btn btn-primary m-2"
                onClick={handleIncrementFive}
            >
                Increase by 5
            </button>
            <button
                className="btn btn-primary m-2"
                onClick={handleDecrementFive}
            >
                Decrease by 5
            </button>
        </div>
    );
};

export default Counter;