import React from 'react';
import ToDoApp from "./containers/ToDoApp/ToDoApp";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Counter from "./containers/Counter/Counter";
import Navigation from "./components/Navigation/Navigation";

const App = () => {
    return (
        <div className="container">
            <BrowserRouter>
                <Navigation/>
                <Switch>
                    <Route exact path={"/"}/>
                    <Route path={"/todoapp"} component={ToDoApp}/>
                    <Route path={"/counterapp"} component={Counter}/>
                </Switch>
            </BrowserRouter>
        </div>
    );
};

export default App;