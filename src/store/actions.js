import axiosApi from "../axiosApi";

export const FETCH_DATA = "FETCH_DATA";
export const FETCH_COUNTER = "FETCH_COUNTER";
export const FETCH_ERROR = "ERROR";

export const fetchData = (payload) => ({type: FETCH_DATA, payload});
export const fetchCounter = (payload) => ({type: FETCH_COUNTER, payload});

export const getCounter = () => async (dispatch) => {
    try {
        const {data} = await axiosApi.get("/counter.json");
        dispatch(fetchCounter(data));
    } catch (e) {
        console.error(e)
    }
}

export const putCounter = (data) => async (dispatch) => {
    try {
        await axiosApi.put("/counter.json", data);
        dispatch(getCounter());
    } catch (e) {
        console.error(e)
    }
};

export const axiosPost = (task) => async (dispatch) => {
    try {
        await axiosApi.post("/tasks.json", task);
        dispatch(axiosGet());
    } catch (e) {
        console.error(e);
    }
}

export const axiosGet = () => async (dispatch) => {
    try {
        const {data} = await axiosApi.get("/tasks.json");
        dispatch(fetchData(data));
    } catch (e) {
        console.error(e)
    }
}

export const axiosRemove = (id) => async (dispatch) => {
    try {
        await axiosApi.delete(`/tasks/${id}.json`);
        dispatch(axiosGet());
    } catch (e) {
        console.error(e)
    }
}



