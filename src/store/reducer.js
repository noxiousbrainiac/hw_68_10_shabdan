import {FETCH_DATA, FETCH_ERROR, FETCH_COUNTER} from "./actions";

const initialState = {
    tasks: [],
    errors: "",
    counter: 0
}

const reducer = (state = initialState, action) => {
    if (action.type === FETCH_DATA) {
        return {...state, tasks: action.payload}
    }

    if (action.type === FETCH_ERROR) {
        return {...state, errors: action.payload}
    }

    if (action.type === FETCH_COUNTER) {
        return {...state, counter: action.payload}
    }

    return state;
};

export default reducer;