import React from 'react';
import ToDoItem from "./ToDoItem/ToDoItem";

const ToDoList = ({tasks, onClick}) => {
    return (
        tasks &&
            <div className="d-flex flex-column">
                {Object.keys(tasks).map(task => (
                    <ToDoItem
                        key={task}
                        onClick={() => onClick(task)}
                        text={tasks[task].task}
                    />
                ))}
            </div>
    );
};

export default ToDoList;