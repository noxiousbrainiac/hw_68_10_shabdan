import React from 'react';

const ToDoItem = ({text, onClick}) => {
    return (
        <div className="card m-1 p-1 d-flex flex-row justify-content-evenly">
            <span className="flex-grow-1">{text}</span>
            <button
                className="btn btn-warning"
                onClick={onClick}
            >
                X
            </button>
        </div>
    );
};

export default ToDoItem;