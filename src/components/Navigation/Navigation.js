import React from 'react';
import {Link} from "react-router-dom";

const Navigation = () => {

    return (
        <ul
            className="d-flex justify-content-evenly"
            style={{listStyle: "none"}}
        >
            <li>
                <Link className="text-decoration-none" to="/todoapp">To-Do</Link>
            </li>
            <li>
                <Link className="text-decoration-none" to="/counterapp">Counter</Link>
            </li>
        </ul>
    );
};

export default Navigation;