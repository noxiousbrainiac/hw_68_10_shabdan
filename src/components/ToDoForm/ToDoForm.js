import React from 'react';

const ToDoForm = ({onSubmit, onChange, value}) => {
    return (
        <form onSubmit={onSubmit}>
            <h1>Add task</h1>
            <div className="d-flex">
                <input
                    value={value}
                    className="form-control mx-1"
                    onChange={onChange}
                    type="text"
                />
                <button
                    className="btn btn-primary"
                    type="submit"
                >
                    Add
                </button>
            </div>
        </form>
    );
};

export default ToDoForm;